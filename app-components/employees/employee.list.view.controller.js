(function () {
    "use strict";

    angular
    .module("HRManagement")
    .controller("EmployeeController", EmployeeController);

    function EmployeeController() {
        var vm = this;
        vm.employeeList = [
            {
                "employeeId": 1,
                "employeeName": "Mark Velo",
                "employeeAge": 28,
                "address": "No 1, abc, UK" 
            },
            {
                "employeeId": 2,
                "employeeName": "Mike Senin",
                "employeeAge": 30,
                "address": "No 2, abc, USA" 
            }
        ];
    }

}());